# Mostrame el branch

## Introducción

Es fundamental que podamos saber en que branch estamos trabajando, para no pisar el ajeno
y poder unificar el código con características nuevas correctamente, así que en vez de ejecutar
`git status` todo el tiempo, este script nos lo muestra en el prompt.

## Script

Agreguemos ésto a nuestro $HOME/.bashrc

```bash
parse_git_branch() {

	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'

}

export PS1="\u@\h \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "
```


Los cambios se aplicarán cuando abramos una consola nueva.
